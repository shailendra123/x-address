import { PipeTransform } from '@angular/core';
import * as i0 from "@angular/core";
export declare class AddressPipe implements PipeTransform {
    show: string;
    transform(value: {}): any;
    static ɵfac: i0.ɵɵFactoryDeclaration<AddressPipe, never>;
    static ɵpipe: i0.ɵɵPipeDeclaration<AddressPipe, "address">;
}
