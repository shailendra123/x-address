import * as i0 from '@angular/core';
import { Injectable, Component, Pipe, NgModule } from '@angular/core';

class PublicService {
    constructor() { }
}
PublicService.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicService, deps: [], target: i0.ɵɵFactoryTarget.Injectable });
PublicService.ɵprov = i0.ɵɵngDeclareInjectable({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicService, providedIn: 'root' });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicService, decorators: [{
            type: Injectable,
            args: [{
                    providedIn: 'root'
                }]
        }], ctorParameters: function () { return []; } });

class PublicComponent {
    constructor() { }
    ngOnInit() {
    }
}
PublicComponent.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicComponent, deps: [], target: i0.ɵɵFactoryTarget.Component });
PublicComponent.ɵcmp = i0.ɵɵngDeclareComponent({ minVersion: "12.0.0", version: "13.2.1", type: PublicComponent, selector: "lib-public", ngImport: i0, template: `
    <p>
      public works!
    </p>
  `, isInline: true });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicComponent, decorators: [{
            type: Component,
            args: [{
                    selector: 'lib-public',
                    template: `
    <p>
      public works!
    </p>
  `,
                    styles: []
                }]
        }], ctorParameters: function () { return []; } });

class AddressPipe {
    constructor() {
        this.show = '';
    }
    transform(value) {
        // return value+'hii value';
        for (const [key, value1] of Object.entries(value)) {
            this.show += value1 + " ,";
            // console.log(`${key}: ${value1}`);
        }
        return this.show;
        // return value;
    }
}
AddressPipe.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: AddressPipe, deps: [], target: i0.ɵɵFactoryTarget.Pipe });
AddressPipe.ɵpipe = i0.ɵɵngDeclarePipe({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: AddressPipe, name: "address" });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: AddressPipe, decorators: [{
            type: Pipe,
            args: [{
                    name: 'address'
                }]
        }] });

class PublicModule {
}
PublicModule.ɵfac = i0.ɵɵngDeclareFactory({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicModule, deps: [], target: i0.ɵɵFactoryTarget.NgModule });
PublicModule.ɵmod = i0.ɵɵngDeclareNgModule({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicModule, declarations: [PublicComponent,
        AddressPipe], exports: [PublicComponent, AddressPipe] });
PublicModule.ɵinj = i0.ɵɵngDeclareInjector({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicModule, imports: [[]] });
i0.ɵɵngDeclareClassMetadata({ minVersion: "12.0.0", version: "13.2.1", ngImport: i0, type: PublicModule, decorators: [{
            type: NgModule,
            args: [{
                    declarations: [
                        PublicComponent,
                        AddressPipe
                    ],
                    imports: [],
                    exports: [
                        PublicComponent, AddressPipe
                    ]
                }]
        }] });

/*
 * Public API Surface of public
 */

/**
 * Generated bundle index. Do not edit.
 */

export { AddressPipe, PublicComponent, PublicModule, PublicService };
//# sourceMappingURL=public.mjs.map
